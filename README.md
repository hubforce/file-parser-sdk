# This is a SDK for working with file parsing service. 

## Installation

You can install the package via composer:

```bash
composer require hubforce/file-parser-sdk
```

Set env variable `PARSING_SERVICE_HOST`

You can publish the config file with:

```bash
php artisan vendor:publish --tag="file-parser-sdk-config"
```

This is the contents of the published config file:

```php
return [
    'host' => ''
];
```

## Usage

```php
$fileParserSdk = new Hubforce\FileParserSdk();
$fileParserSdk->parseFileByUrl($url)
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
