<?php

namespace Hubforce\FileParserSdk\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Hubforce\FileParserSdk\FileParserSdk
 */
class FileParserSdk extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \Hubforce\FileParserSdk\FileParserSdk::class;
    }
}
