<?php

namespace Hubforce\FileParserSdk;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileParserSdk
{
    public function __construct(
        private readonly Client $client,
        private readonly UrlService $urlService
    )
    {}

    public function parseFileContentByUrl(string $url): array
    {
        return $this->getResult(
            $this->sendRequest(Request::METHOD_POST, $this->urlService->parseEndpoint(), [
                'file_url' => $url
            ])
        );
    }

    public function sendRequest(string $method, string $url, array $parameters = []): ResponseInterface
    {
        $requestParams = [
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ];

        if ($parameters) {
            $requestParams['json'] = $parameters;
        }

        return $this->client->request($method, $url, $requestParams);
    }

    public function getResult(ResponseInterface $response): array
    {
        $statusCode = $response->getStatusCode();

        if (!in_array($statusCode, [Response::HTTP_CREATED, Response::HTTP_OK])) {
            throw new \Exception('Invalid status code: ' . $statusCode);
        }

        $result = json_decode($response->getBody()->getContents(), true);

        if (!$result) {
            throw new \Exception('Invalid response.');
        }

        return $result;
    }
}
