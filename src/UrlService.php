<?php

namespace Hubforce\FileParserSdk;

class UrlService
{
    public function parseEndpoint(): string
    {
        return $this->getHost().'/api/parse_file_content';
    }

    private function getHost(): string
    {
        return config('file_parser_sdk.host');
    }
}